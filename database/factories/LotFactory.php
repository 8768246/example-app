<?php

namespace Database\Factories;

use App\Dto\LotContentDto;
use Illuminate\Database\Eloquent\Factories\Factory;
use Spatie\DataTransferObject\Exceptions\UnknownProperties;

class LotFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     * @throws UnknownProperties
     */
    public function definition()
    {
        $faker = \Faker\Factory::create();

        $content = [
            'price' => $faker->numberBetween(10000,200000),
            'rooms' => $faker->numberBetween(1,6),
            'space' => $faker->numberBetween(20,250),
            'beds' => $faker->numberBetween(1,10),
            'animals' => $faker->boolean(20),
            'smoking' => $faker->boolean(10),
            'parking' => $faker->boolean(80),
        ];

        return [
            'content' => new LotContentDto($content),
        ];
    }
}
