<?php

namespace Tests\Feature;

use App\Models\Lot;
use App\Models\Order;
use App\Services\MatchServiceInterface;
use Tests\TestCase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->get('/');

        /** @var MatchServiceInterface $MS */
        $MS = app()->get(MatchServiceInterface::class);

        //$MS->processOrder(Order::first());
        $MS->processLot(Lot::first());

        $response->assertStatus(200);
    }
}
