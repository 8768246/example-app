<?php

namespace Tests\Unit\Rules;

use App\Dto\FilterDto;
use App\Services\MatchService\Rules\RelativePriceRule;
use PHPUnit\Framework\TestCase;

class ExampleTest extends TestCase
{

    /**
     * @dataProvider tableData
     */
    public function test_example($name, $scopes)
    {
        [$a, $b, $result] = $scopes;

        $rule = new RelativePriceRule([], 2, 'price');

        $diff = $rule->compare(
            new FilterDto(['price' => $a]),
            new FilterDto(['price' => $b]),
        );

        $this->assertEquals($result, $diff);
    }

    /**
     * @dataProvider tableData
     */
    public function tableData()
    {
        return [
            [1, [30000,30000,0]],
            [2, [30000,35000,1]],
            [3, [-5000,5000,2]],
        ];
    }
}
