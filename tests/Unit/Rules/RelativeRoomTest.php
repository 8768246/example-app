<?php

namespace Tests\Unit\Rules;

use App\Dto\FilterDto;
use App\Services\MatchService\Rules\RelativePriceRule;
use App\Services\MatchService\Rules\RelativeRoomRule;
use PHPUnit\Framework\TestCase;

class RelativeRoomTest extends TestCase
{
    /**
     * @dataProvider tableData
     */
    public function test_example($name, $scopes)
    {
        [$a, $b, $result] = $scopes;

        $rule = new RelativeRoomRule([]);

        $diff = $rule->compare(
            new FilterDto(['rooms' => $a]),
            new FilterDto(['rooms' => $b]),
        );

        $this->assertEquals($result, $diff);
    }

    /**
     * @dataProvider tableData
     */
    public function tableData()
    {
        return [
            [1, [3,4,5]],
            [1, [4,3,5]],
            [1, [5,2,3*5]],
        ];
    }
}
