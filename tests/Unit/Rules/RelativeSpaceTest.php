<?php

namespace Tests\Unit\Rules;

use App\Dto\FilterDto;
use App\Services\MatchService\Rules\RelativePriceRule;
use App\Services\MatchService\Rules\RelativeRoomRule;
use App\Services\MatchService\Rules\RelativeSpaceRule;
use PHPUnit\Framework\TestCase;

class RelativeSpaceTest extends TestCase
{
    /**
     * @dataProvider tableData
     */
    public function test_example($name, $scopes)
    {
        [$a, $b, $result] = $scopes;

        $rule = new RelativeSpaceRule([]);

        $diff = $rule->compare(
            new FilterDto(['space' => $a]),
            new FilterDto(['space' => $b]),
        );

        $this->assertEquals($result, $diff);
    }

    /**
     * @dataProvider tableData
     */
    public function tableData()
    {
        return [
            [1, [450,450, 0]],
            [2, [450,455, 1]],
            [2, [100,200, 20]],
            [2, [200,100, 20]],
        ];
    }
}
