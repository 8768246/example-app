<?php

namespace App\Repository;

use App\Dto\LotDto;
use App\Dto\Pagination;
use App\Models\Lot;
use Illuminate\Support\Collection;

class LotRepository
{
    public function createLot(LotDto $dto): int {
        $item = Lot::insirt([
            'filter' => $dto->content
        ]);

        return $item->id;
    }

    public function updateLotById($id, LotDto $dto): int {
        $item = Lot::where('id',$id)->update([
            'filter' => $dto->content
        ]);

        return $item->id;
    }

    public function getLots(int $page) {
        //@todo: write selects

        $res = Lot::orderBy('id','desc')->limit(50)->paginate();

        return new Pagination([
            'items' => $res->items(),
            'total' => $res->total(),
            'last_page' => $res->lastPage(),
        ]);
    }

    public function deleteLotById(int $id): void {
        Lot::where('id',$id)->delete();
    }
}
