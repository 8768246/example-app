<?php

namespace App\Http\Controllers;

use App\Dto\LotDto;
use App\Http\Requests\LotStoreRequest;
use App\Http\Requests\OrderStoreRequest;
use App\Models\Lot;
use App\Models\Order;
use App\Repository\LotRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class LotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(LotRepository $lot_repository)
    {
        return $lot_repository->getLots(0)->toArray();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LotStoreRequest $request, LotRepository $lot_repository)
    {
        $lo_dto = new LotDto([
            'name' => $request->name,
            'filter' => [
                'price' => $request->price,
                'animals' => $request->animals,
                'smoke' => $request->smoke,
                'rooms' => $request->rooms,
                'space' => $request->space,
                'beds' => $request->beds,
                'parking' => $request->parking,
            ]
        ]);

        $lot_repository->createLot($lo_dto);
    }

    /**
     * Update the specified resource in storage.
     *destroy
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Lot  $lot
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Lot $lot, LotRepository $lot_repository)
    {
        $lo_dto = $request->toDto(LotDto::class);
        $lot_repository->updateLotById($lot->id, $lo_dto);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Lot  $lot
     * @return JsonResponse
     */
    public function destroy(Lot $lot, LotRepository $lot_repository): JsonResponse
    {
        $lot_repository->deleteLotById($lot->id);
        return Response::json(['success' => true]);
    }
}
