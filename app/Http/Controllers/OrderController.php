<?php

namespace App\Http\Controllers;

use App\Dto\FilterDto;
use App\Http\Requests\OrderStoreRequest;
use App\Http\Requests\OrderUpdateRequest;
use App\Jobs\MatchOrder;
use App\Models\Order;
use App\Services\MatchServiceInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Response;

class OrderController extends Controller
{
    /**
     * @param Order $order
     * @param OrderUpdateRequest $request
     * @return JsonResponse
     */
    public function update(Order $order, OrderUpdateRequest $request): JsonResponse
    {

        $filter_dto = $request->toDto(FilterDto::class);

        $order->fill([
            'name' => $request->name,
            'filter' => $filter_dto->toArray(),
        ]);

        if ($dirty = $order->isDirty()) {
            $order->save();
            MatchOrder::dispatch($order);
        }

        return Response::json([
            'dirty' => $dirty,
            'id' => $order->id,
            'filter' => $filter_dto,
        ], 200);
    }

    /**
     * @param OrderStoreRequest $request
     * @param MatchServiceInterface $match_service
     * @return JsonResponse
     */
    public function store(OrderStoreRequest $request): JsonResponse {

        $filter_dto = $request->toDto(FilterDto::class);

        $order = Order::create([
            'filter' => $filter_dto->toArray()
        ]);

        MatchOrder::dispatch($order);

        return Response::json([
            'id' => $order->id,
            'filter' => $filter_dto,
        ], 200);
    }

    /**
     * @param Order $order
     * @return JsonResponse
     */
    public function delete(Order $order): JsonResponse {

        $order->delete();

        return Response::json([
            'id' => $order->id,
        ], 200);
    }
}
