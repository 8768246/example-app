<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Spatie\DataTransferObject\DataTransferObject;

abstract class BaseRequest extends FormRequest
{
    /**
     * @param DataTransferObject $dto
     * @return DataTransferObject
     */
    public function toDto(string $dto): DataTransferObject
    {
        return new $dto($this->validationData());
    }
}
