<?php

namespace App\Services;

use App\Models\Lot;
use App\Models\Order;

interface MatchServiceInterface
{
    public function processAll();
    public function processOrder(Order $order);
    public function processLot(Lot $order);
}
