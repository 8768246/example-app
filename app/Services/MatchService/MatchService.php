<?php

namespace App\Services\MatchService;

use App\Dto\FilterDto;
use App\Models\Lot;
use App\Models\Order;
use App\Services\MatchService\Rules\RelativePriceRule;
use App\Services\MatchService\Rules\RelativeRoomRule;
use App\Services\MatchService\Rules\RelativeSpaceRule;
use App\Services\MatchServiceInterface;
use Illuminate\Support\Facades\Validator;
use Spatie\DataTransferObject\Exceptions\UnknownProperties;

class MatchService implements MatchServiceInterface
{
    private int $scope_initial;
    private int $scope_min;

    public function __construct()
    {
        $this->scope_initial = config('services.match.score_max', 100);
        $this->scope_min = config('services.match.score_min', 85);
        $this->max_matches_per_order = config('services.match.max_matches_per_order', 85);
    }

    protected function validateRule($a, $b, $column, $rules) {
        $validator = Validator::make([
            'a' => $a[$column] ?? null,
            'b' => $b[$column] ?? null,
        ], $rules);

        return $validator;
    }

    private function getScore(array $pipe, $filter, $lot): int {
        $scope_initial = $this->scope_initial;

        foreach ($pipe as $rule) {
            $scope_diff = $rule->compare(
                new FilterDto($lot->content),
                new FilterDto($filter)
            );

            $score_final = $scope_initial - $scope_diff;
        }

        return $score_final;
    }

    /**
     * @return \Generator
     */
    public function lotGenerator(): \Generator
    {
        /** @var Lot $lot */
        foreach (Lot::orderBy('id','asc')->cursor() as $lot) {
            yield $lot;
        }
    }

    /**
     * @return \Generator
     */
    public function orderGenerator(): \Generator
    {
        /** @var Lot $lot */
        foreach (Order::orderBy('id','asc')->cursor() as $order) {
            yield $order;
        }
    }

    /**
     * @throws UnknownProperties
     */
    public function match($orders, $lots, bool $ink): \Generator {

        $pipe = [
            new RelativePriceRule([]),
            new RelativeRoomRule([]),
            new RelativeSpaceRule([]),
        ];

        foreach ($orders as $order) {
            $filter = $order->filter;

            $found = 0;
            $found_lot_ids = $ink ? ($order->match ?? []) : [];

            foreach ($lots as $lot) {
                $score_final = $this->getScore($pipe, $filter, $lot);

                if ($score_final >= $this->scope_min) {
                    $found++;
                    $found_lot_ids[] = $lot->id;

                    if ($this->max_matches_per_order <= $found) {
                        break;
                    }
                }
            }

            yield $order => $found_lot_ids;
        }
    }

    /**
     * @param Order $order
     * @throws UnknownProperties
     */
    public function processOrder(Order $order) {
        foreach ($this->match($order, $this->lotGenerator(), false) as $matched_ids) {
            $this->updateOrderMatch($order, $matched_ids);
        }
    }

    /**
     * @param Lot $lot
     * @throws UnknownProperties
     */
    public function processLot(Lot $lot) {
        foreach ($this->match($this->orderGenerator(), [$lot], true) as $order => $matched_ids) {
            $this->updateOrderMatch($order, $matched_ids);
        }
    }

    public function processAll() {
        foreach ($this->orderGenerator() as $order) {
            foreach ($this->match([$order], $this->lotGenerator(), false) as $order => $matched_ids) {
                $this->updateOrderMatch($order, $matched_ids);

                yield $order => $matched_ids;
            }
        }
    }

    protected function updateOrderMatch(Order $order, array $matched_ids): void {
        $order->match = $matched_ids;
        if ($order->isDirty()) {
            $order->save();
        }
    }
}
