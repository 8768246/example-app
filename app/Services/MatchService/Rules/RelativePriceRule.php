<?php

namespace App\Services\MatchService\Rules;

use App\Dto\FilterDto;
use Illuminate\Database\Eloquent\Model;

class RelativePriceRule
{
    public function __construct(
        protected array  $options,
        protected int    $score = 4,
        protected string $column = 'price'
    ) {}

    public function rule(): array {
        return [
            'a' => 'min:0|integer|required',
            'b' => 'min:0|integer|required',
        ];
    }

    public function compare(FilterDto $a, FilterDto $b): int {

        $price_a = $a->{$this->column};
        $price_b = $b->{$this->column};

        $diff = ceil(($price_a - $price_b) / 5000);

        return abs($diff);
    }
}
