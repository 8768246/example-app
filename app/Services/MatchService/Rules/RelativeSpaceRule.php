<?php

namespace App\Services\MatchService\Rules;

use App\Dto\FilterDto;
use Illuminate\Database\Eloquent\Model;

class RelativeSpaceRule
{
    public function __construct(
        protected array  $options,
        protected int    $score = 1,
        protected string $column = 'space'
    ) {}

    public function converter($a, $b): array {
        return [$a[$this->column], $b[$this->column]];
    }

    public function compare(FilterDto $a, FilterDto $b): int {

        $value_a = $a->{$this->column};
        $value_b = $b->{$this->column};

        $diff = floor(($value_a - $value_b) / 5) * $this->score;

        return abs($diff);
    }
}
