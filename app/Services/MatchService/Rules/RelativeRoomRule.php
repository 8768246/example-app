<?php

namespace App\Services\MatchService\Rules;

use App\Dto\FilterDto;
use Illuminate\Database\Eloquent\Model;

class RelativeRoomRule
{
    public function __construct(
        protected array  $options,
        protected int    $score = 5,
        protected string $column = 'rooms'
    ) {}

    public function compare(FilterDto $a, FilterDto $b): int {

        $value_a = $a->{$this->column};
        $value_b = $b->{$this->column};

        $diff = ceil(($value_a - $value_b) * $this->score);

        return abs($diff);
    }
}
