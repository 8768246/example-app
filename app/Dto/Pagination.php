<?php

namespace App\Dto;

use Illuminate\Support\Collection;
use Spatie\DataTransferObject\DataTransferObject;

class Pagination extends DataTransferObject
{
    public $items;

    #[MapFrom('total()')]
    public ?int $total;
    public ?int $last_page;
}
