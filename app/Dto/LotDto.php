<?php

namespace App\Dto;

use Spatie\DataTransferObject\DataTransferObject;

class LotDto extends DataTransferObject
{
    public string $name;
    public FilterDto $content;
}
