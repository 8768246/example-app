<?php

namespace App\Dto;

use Spatie\DataTransferObject\DataTransferObject;

class FilterDto extends DataTransferObject
{
    public ?bool $animals;
    public ?bool $smoke;

    #[NumberBetween(1, 10000000)]
    public ?int  $price;

    public ?int  $rooms;
    public ?int  $space;
    public ?int  $beds;
    public ?bool $parking;
}
